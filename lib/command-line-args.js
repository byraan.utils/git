const commandLineArgs=require('command-line-args');
process.arguments={};
process.options=[];
module.exports=function(parent_exports, options, subcommands){
	if(typeof parent_exports!='object' || parent_exports.constructor.name!='Object') return;
	if(typeof options!='object' || options.constructor.name!='Array') options=[];
	for(let i=0; i<options.length; i++){
		let name=options[i].name;
		if(!name) name=options[i].alias;
		if(!name) continue;
		if(process.options.indexOf(name)>=0){
			options.splice(i, 1);
			continue;
		}
		process.options.push(name);
	}
	var argv;
	if(typeof process.arguments._unknown=='object') argv=process.arguments._unknown;
	var args=commandLineArgs(options, {
		argv,
		partial: true
	});
	parent_exports.subcommands=[];
	if(typeof args._unknown=='object'){
		let i=0;
		if(subcommands===true) subcommands=args._unknown.length;
		if(typeof subcommands!='number') subcommands=1;
		do{
			if(args._unknown[0].charAt(0)=='-'){
				if(parent_exports.subcommands.length) break;
				args._unknown.splice(0, 1);
				continue;
			}
			parent_exports.subcommands.push(args._unknown[0]);
			args._unknown.splice(0, 1);
			i++;
			if(i>=subcommands) break;
		}while(args._unknown.length);
	}
	process.arguments._unknown=[];
	process.arguments=Object.assign({}, process.arguments, args);
};